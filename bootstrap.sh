#!/bin/bash

GREEN='\033[0;32m'
gecho() {
  echo -e "$GREEN $1"
}
is_installed() {
  [ "$(dpkg -s $1 | grep Status)" == "Status: install ok installed" ]
}
install_package() {
  # Check if the package is already present
  if is_installed $1; then
    gecho "$1 is already installed. Proceeding.."
  else
    sudo apt install $1 -y
    gecho "Succesfully install $1"
  fi
}

gecho "Remove the Firefox Snap package"
sudo snap disable firefox
sudo snap remove --purge firefox
gecho "Firefox removed"

for package_name in neovim ripgrep build-essential git curl fzf htop zsh age gnupg software-properties-common ca-certificates apt-transport-https tmux; do
  install_package $package_name
done

gecho "Setting zsh as the default shell"
sudo chsh -s $(which zsh) richard
gecho "Zsh succesfully set as default shell"

gecho "Installing ohmyzsh"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
gecho "ohmyzsh installed succesfully"

gecho "Installing FluxCD"
curl -s https://fluxcd.io/install.sh | sudo bash
gecho "FluxCD installed succesfully"

if is_installed google-chrome-stable; 
then
  gecho "Chrome is already installed. Proceeding.."
else
  wget -O chrome.deb https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
	sudo dpkg -i chrome.deb
	rm chrome.deb
fi


if is_installed kubectl; 
then
  gecho "Kubectl is already installed. Proceeding.."
else
	curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.28/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
	echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
	sudo apt update
	sudo apt install -y kubectl
fi

echo "Install 1Password"
if is_installed 1password; 
then
  gecho "1Password is already installed. Proceeding.."
else
	curl -sS https://downloads.1password.com/linux/keys/1password.asc | sudo gpg --dearmor --output /usr/share/keyrings/1password-archive-keyring.gpg
	echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/1password-archive-keyring.gpg] https://downloads.1password.com/linux/debian/amd64 stable main' | sudo tee /etc/apt/sources.list.d/1password.list
	mkdir -p /etc/debsig/policies/AC2D62742012EA22/
	curl -sS https://downloads.1password.com/linux/debian/debsig/1password.pol | sudo tee /etc/debsig/policies/AC2D62742012EA22/1password.pol
	mkdir -p /usr/share/debsig/keyrings/AC2D62742012EA22
	curl -sS https://downloads.1password.com/linux/keys/1password.asc | sudo gpg --dearmor --output /usr/share/debsig/keyrings/AC2D62742012EA22/debsig.gpg
	sudo apt update
	sudo apt install -y 1password
fi

gecho "Install Slack"
sudo snap install slack
gecho "Slack installed"

gecho "Install Tailscale"
curl -fsSL https://tailscale.com/install.sh | sh
gecho "Tailscale installed"

echo "Install i3"
if is_installed i3; 
then
  gecho "i3 is already installed. Proceeding.."
else
	/usr/lib/apt/apt-helper download-file https://debian.sur5r.net/i3/pool/main/s/sur5r-keyring/sur5r-keyring_2023.02.18_all.deb keyring.deb SHA256:a511ac5f10cd811f8a4ca44d665f2fa1add7a9f09bef238cdfad8461f5239cc4
	sudo apt install ./keyring.deb
	echo "deb http://debian.sur5r.net/i3/ $(grep '^DISTRIB_CODENAME=' /etc/lsb-release | cut -f2 -d=) universe" | sudo tee /etc/apt/sources.list.d/sur5r-i3.list
	sudo apt update
	sudo apt install i3
fi

gecho "Install Golang"
sudo rm -rf /usr/local/go && sudo tar -C /usr/local -xzf go1.21.0.linux-amd64.tar.gz
export PATH=$PATH:/usr/local/go/bin
gecho "Golang installed"

echo "Install Terraform"
if is_installed terraform; 
then
  gecho "Terraform is already installed. Proceeding.."
else
	wget -O- https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
	echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
	sudo apt update
	sudo apt-get install terraform -y
fi

gecho "Add the JetBrainsFont"
wget -O jetbrainsmono.zip https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/JetBrainsMono.zip
unzip jetbrainsmono.zip -d ~/.fonts
fc-cache -fv
rm jetbrainsmono.zip

Gecho "Install SOPS"
if is_installed sops; 
then
  gecho "SOPS is already installed. Proceeding.."
else
	wget -O sops.deb https://github.com/getsops/sops/releases/download/v3.7.3/sops_3.7.3_amd64.deb
	sudo dpkg -i sops.deb
	rm sops.deb
fi

git config --global user.name "Richard van der Veen"
git config --global user.email "admin@richardvanderveen.com"

git clone https://github.com/NvChad/NvChad ~/.config/nvim --depth 1 && nvim


